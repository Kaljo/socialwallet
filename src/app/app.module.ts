import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { UsersPage } from '../pages/users/userPage';
import { ProductsPage } from '../pages/products/productPage';
import { ExpandableHeader } from '../components/expandable-header/expandable-header';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DragulaModule } from 'ng2-dragula';
import { UserService } from '../pages/users/user.service';
import { ProductService } from '../pages/products/product.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    UsersPage,
    ExpandableHeader,
    ProductsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    DragulaModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    UsersPage,
    ExpandableHeader,
    ProductsPage
  ],
  providers: [
    UserService,
    ProductService,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
