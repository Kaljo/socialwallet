import { Injectable } from '@angular/core';
import Product from '../../model/product/product';

@Injectable()
export class ProductService {

    private static productID: number = 0;

    products: Array<Product>;
 
    constructor() {
        this.products = new Array<Product>();
    }

    addProduct(formInput, editMode: boolean) {
        let product;
        if (editMode) {
            let index = this.products.findIndex((product) => {
                return product.id === formInput.id;
            });
            product = new Product(formInput.id, formInput.name, Number(formInput.price), Number(formInput.quantity), formInput.icon);
            this.products[index] = product;
        } else {
            product = new Product(++ProductService.productID, formInput.name, Number(formInput.price), Number(formInput.quantity), formInput.icon);
            this.products.push(product);
        }
        return product;
    }

    getProducts() {
        return this.products;
    }

    getProductById(id: number) {
        return this.products.find((product) => {
            return product.id === id;
        });
    }

}