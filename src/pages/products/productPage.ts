import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ViewController, FabContainer, NavParams } from 'ionic-angular';
import Product from '../../model/product/product';
import { ProductService } from './product.service';

@Component({
	selector: 'products-page',
	templateUrl: 'productPage.html'
})
export class ProductsPage {

	productForm: FormGroup;

	editMode: boolean;

	constructor(public view: ViewController, public formBuilder: FormBuilder, public productService: ProductService, params: NavParams) {
		let formData;
		
		if (params.data.product) {
			formData = params.data.product;
			this.editMode = true;
		} else {
			formData = {
				id: 0,
				name: '',
				price: '',
				quantity: '',
				icon: ''
			};
			this.editMode = false;
		}

		this.productForm = formBuilder.group({
			id: formData.id,
			name: [formData.name, Validators.required],
			price: ['', Validators.compose([Validators.required, Validators.min(0), Validators.pattern(/^\d+\.?\d*$/)])],
			quantity: ['', Validators.compose([Validators.required, Validators.min(1), Validators.pattern(/^\d+$/)])],
			icon: formData.icon
		});
	}

	addProduct(formInput) {
		let product = this.productService.addProduct(formInput, this.editMode);
		this.view.dismiss(product);
	}

	close() {
		this.view.dismiss();
	}

	isFieldValid(controlName: string) {
		let formControl = this.productForm.controls[controlName];
		return !formControl.valid && formControl.touched;
	}

	setMyIcon(settingIcon: string, fabContainer: FabContainer) {
		this.productForm.controls["icon"].setValue(settingIcon);
		fabContainer.close();
	}

}
