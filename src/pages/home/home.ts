import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';
import { DragulaService } from '../../../node_modules/ng2-dragula/ng2-dragula';
import { UsersPage } from '../users/userPage';
import { ProductsPage } from '../products/productPage';
import { UserService } from '../users/user.service';
import { ProductService } from '../products/product.service';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	public users = [];
	public messages = [];
	public products = [];

	constructor(public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, public dragulaService: DragulaService, public userService: UserService, private productService: ProductService) {

	};

	addUser() {
		let addModal = this.modalCtrl.create(UsersPage);
		addModal.onDidDismiss((item) => {
			if (item) {
				this.users.push(item);
			}
		});

		addModal.present();
	}

	editUser(id: number) {
		let editModal = this.modalCtrl.create(UsersPage, { user: this.userService.getUserById(id)});
		editModal.onWillDismiss((item) => {
			if (item) {
				let index = this.users.findIndex((user) => {
					return user.id === item.id;
				});
				this.users[index] = item;
			}
		});
		editModal.present();
	}

	addProduct() {
		let addModal = this.modalCtrl.create(ProductsPage);
		addModal.onDidDismiss((item) => {
			if (item) {
				this.products.push(item);
			}
		});
		addModal.present();
	};

	editProduct(id: number) {
		let editModal = this.modalCtrl.create(ProductsPage, {product: this.productService.getProductById(id)});
		editModal.onWillDismiss((item) => {
			if (item) {
				let index = this.products.findIndex((product) => {
					return product.id === item.id;
				});
				this.products[index] = item;
			}
		});
		editModal.present();
	}

	ngOnInit() {
		this.dragulaService.drop.subscribe((value) => {

			if (value[3].id == 1 && value[2].id == 2) {  // 1 - all products; 2 - user prodects
				for (let u of this.users) {
					for (let p of u.products) {
						if (p.id == value[1].value && !p.isAssignedToSb) {
							if (Number(p.quantity) > 1) {
								let copyProduct = {
									name: p.name,
									myIcon: p.myIcon,
									price: Number((Math.round(Number(p.price) / Number(p.quantity) * (Number(p.quantity) - 1) * 100) / 100).toFixed(2)),
									quantity: (Number(p.quantity) - 1).toString(),
									isAssignedToSb: false,
									id: p.id
								};

								p.price = Number(p.price) / Number(p.quantity);
								p.price = Number((Math.round(Number(p.price) * 100) / 100).toFixed(2));
								p.quantity = "1";
								this.products.push(copyProduct);
							}

							p.isAssignedToSb = true;
							break;
						}
					}
				}
				let p_to_be_deleted: number = -1;
				let u_to_be_deleted: number = -1;
				let p_index: number = 0;
				let u_index: number = 0;

				for (let u of this.users) {
					for (let p1 of u.products) {
						for (let p2 of u.products) {
							if (p1.id == value[1].value && p2.id == value[1].value && p1 != p2) {

								p2.price = Number(p2.price) / Number(p2.quantity) * (Number(p2.quantity) + Number(p1.quantity));
								p2.price = Number((Math.round(Number(p2.price) * 100) / 100).toFixed(2));
								p2.quantity = Number(p2.quantity) + Number(p1.quantity);

								p_to_be_deleted = p_index; // index of p1
								u_to_be_deleted = u_index;

								break;
							}
							if (u_to_be_deleted != -1 && p_to_be_deleted != -1) break;
						}
						if (u_to_be_deleted != -1 && p_to_be_deleted != -1) break;
						p_index++;
					}
					if (u_to_be_deleted != -1 && p_to_be_deleted != -1) break;
					u_index++;
				}

				if (u_to_be_deleted != -1 && p_to_be_deleted != -1) {
					this.users[u_to_be_deleted].products.splice(p_to_be_deleted, 1);
				}
			}
			else if (value[3].id == 2 && value[2].id == 1) {   // 1 - all products; 2 - user prodects
				let to_be_deleted: number = -1;
				let index: number = 0;

				for (let p1 of this.products) {
					for (let p2 of this.products) {
						if (p2.id == value[1].value) {
							p2.isAssignedToSb = false;
							if (p1.id == value[1].value && Number(p1.quantity) == 1 && p1 != p2) {

								p2.price = Number(p2.price) / Number(p2.quantity) * ((Number(p2.quantity) + 1));
								p2.price = Number((Math.round(Number(p2.price) * 100) / 100).toFixed(2));
								p2.quantity = Number(p2.quantity) + 1;
								p2.isAssignedToSb = false;
								to_be_deleted = index;

								break;
							}
						}
					}
					index++;
				}

				if (to_be_deleted != -1) {
					this.products.splice(to_be_deleted, 1);
				}
			}
			else console.log("Drop from " + value[3].id + " to " + value[2].id);

			for (let u of this.users) {
				let sum: number = 0;
				for (let p of u.products) {
					sum = sum + Number(p.price);
				}
				u.hasGiven = Number((Math.round(Number(sum) * 100) / 100).toFixed(2));
			}
		});
	}

	calculate() {

		let bill = 0;
		let trenutni: any;
		for (let u of this.users) {
			bill += u.expenses;
		};

		//let brojLjudi = this.users.length;

		for (let u of this.users) {
			u.status = (u.expenses - u.hasGiven);
		};

		//ako svi statusi u zbiru nisu = 0 onda znaci da ima greske pri unosu cena
		let zbirStatusa = 0;
		for (let u of this.users)
			zbirStatusa += u.status;

		if (zbirStatusa != 0) {
			console.log("Greska pri unosu cena!");
			return;
		}
		let i = 0;
		let j = 0;
		//I nacin raskusuravanja, ako tacno ima da se raskusura!!!!
		while (i < this.users.length) {
			while (i < this.users.length && this.users[i].status == 0) {
				i++;
			}
			if (i < this.users.length) {
				trenutni = this.users[i];
			}
			else {
				break;
			}
			//trazimo onog u this.usersu koji ima tacno onoliko koliko -trenutni da se odmah raskusuraju
			let trazimo = -trenutni.status;
			let j = i;
			while (j < this.users.length && this.users[j].status != trazimo) {
				j++;
			}
			if (j < this.users.length) {
				//nasli smo coveka koji ima tacno koliko fali
				let trenutni2 = this.users[j];
				//trenutni i trenutni2 treba da zamene pare
				//onaj koji je u plusu treba da da pare
				if (trenutni.status > 0) {
					console.log(trenutni.name + " treba da da  " + trenutni.status + "din. " + trenutni2.name);
					this.messages.push(trenutni.name + " treba da da  " + trenutni.status + "din. " + trenutni2.name);
				}
				else {
					console.log(trenutni2.name + " treba da da  " + trenutni2.status + "din. " + trenutni.name);
					this.messages.push(trenutni2.name + " treba da da  " + trenutni2.status + "din. " + trenutni.name);
				}
				trenutni.status = 0;
				trenutni2.status = 0;
			}
			i++;
		}
		//-----------------------------------------------------------------------------------------------------------
		//II nacin za raskusuravanje ukoliko postoji element u this.usersu kome status != 0
		//provera da li treba raditi ovo
		i = 0;
		let raditi2deo = 0;
		for (i = 0; i < this.users.length; i++) {
			if (this.users[i].status != 0) {
				raditi2deo = 1;
			}
		}
		if (raditi2deo) {
			i = 0;
			while (i < this.users.length) {
				//trazimo prvi element kome je status>0
				while (i < this.users.length && this.users[i].status <= 0) {
					i++;
				}
				if (i < this.users.length) {
					trenutni = this.users[i];
				}
				else {
					break;
				}
				let trazimo = -trenutni.status;
				//trazimo nekog ko ima status<trazimo
				j = 0;
				while (j < this.users.length && this.users[j].status > trazimo) {
					j++;
				}
				if (j < this.users.length) {
					//znaci nasli smo nekoga ciji je status<trazimo
					let novi = this.users[j];
					novi.status += trenutni.status;
					console.log(trenutni.name + " treba da da " + trenutni.status + "din. " + novi.name);
					this.messages.push(trenutni.name + " treba da da " + trenutni.status + "din. " + novi.name);
					i = -1;
					trenutni.status = 0;
				}
				i++;
			}
		}
		//-----------------------------------------------------------------------------------------------------------
		//Provera da li treba raditi i na treci nacin!!!
		i = 0;
		let raditi3deo = 0;
		for (i = 0; i < this.users.length; i++) {
			if (this.users[i].status != 0) {
				raditi3deo = 1;
			}
		}
		if (raditi3deo) {
			//znaci postoji mnogo ljudi po malo u minusu a neko mnogo u plusu
			//trazim prvog coveka u minusu i neko ko je u vecem plusu
			i = 0;
			while (i < this.users.length) {
				while (i < this.users.length && this.users[i].status >= 0) {
					i++;
				}
				if (i < this.users.length) {
					trenutni = this.users[i];
				}
				else {
					break;
				}
				let trazimo = -trenutni.status;
				//trazimo coveka status>trazimo
				j = 0;
				while (j < this.users.length && this.users[j].status < trazimo) {
					j++;
				}
				if (j < this.users.length) {
					let novi = this.users[j];
					console.log(novi.name + " treba da da " + trazimo + "din. " + trenutni.name);
					this.messages.push(novi.name + " treba da da " + trazimo + "din. " + trenutni.name);
					novi.status -= trazimo;
					trenutni.status = 0;
					i = -1;
				}
				i++;
			}
		}
	};

}
