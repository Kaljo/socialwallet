import { Injectable } from '@angular/core';
import User from '../../model/user/user';

@Injectable()
export class UserService {

    private static userID: number = 0;

    users: Array<User>;

    constructor() {
        this.users = new Array<User>();
    }

    addUser(formInput, editMode: boolean) {
        let user;
        if (editMode) {
            let index = this.users.findIndex((user) => {
                return user.id === formInput.id;
            });
            user = new User(formInput.id, formInput.name, Number(formInput.hasGiven));
            this.users[index] = user;
        } else {
            user = new User(++UserService.userID, formInput.name, Number(formInput.hasGiven));
            this.users.push(user);
        }
        return user;
    }

    getUsers() {
        return this.users;
    }

    getUserById(id: number) {
        return this.users.find((user) => {
            return user.id === id;
        });
    }

}