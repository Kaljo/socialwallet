import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ViewController, NavParams} from 'ionic-angular';
import { UserService } from './user.service';

@Component({
  selector: 'page-users',
  templateUrl: 'userPage.html'
})
export class UsersPage {

	userForm: FormGroup;

	editMode: boolean;

	constructor(public view: ViewController, public formBuilder: FormBuilder, public userService: UserService, params: NavParams) {

		let formData;
		if (params.data.user) {
			formData = {
				id: params.data.user.id,
				name: params.data.user.name,
				hasGiven: Number(params.data.user.hasGiven),
				edit: true
			};
		} else {
			formData = {
				id: 0,
				name: '',
				hasGiven: '',
				edit: false
			}
		}

		this.editMode = formData.edit;

		this.userForm = formBuilder.group({
			id: formData.id,
			name: [formData.name, Validators.required],
			hasGiven: [formData.hasGiven, Validators.compose([Validators.required, Validators.min(0), Validators.pattern(/^\d+\.?\d*$/)])]
		});
	}

	addUser(formInput) {
		let user = this.userService.addUser(formInput, this.editMode);
		this.view.dismiss(user);
	}

	isFieldValid(controlName: string) {
    let formControl = this.userForm.controls[controlName];
    return !formControl.valid && formControl.touched;
  }

  close(){
    this.view.dismiss();
  }
}
