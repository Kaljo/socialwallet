import Product from '../product/product';

export default class User {
    name: string;
    hasGiven: number;
    expenses: number;
    id: 0;
    products: Array<Product>;

    constructor(id, name: string, hasGiven: number) {
        this.id = id;
				this.name = name;
        this.hasGiven = hasGiven;
        this.products = new Array<Product>();
    }
}