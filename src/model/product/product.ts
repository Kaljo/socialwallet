export default class Product {
    id: number = 0;
    icon: string = "";
    name: string = "";
    price: number = 0;
    quantity: number = 0;
    isAssignedToSb: boolean = false;

    constructor (id:number, name: string, price: number, quantity: number, icon: string) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.icon = icon;
    }

}