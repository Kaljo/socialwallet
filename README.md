******GIT WORKFLOW ********

- NO BRANCHING

For those who don't want to use branches, only work (commit, push, pull) on master branch.

After starting a task commit after every small functionality is done, if possible.
Push to master when whole task is done.
Pull from master frequqntly.

- BRANCHING

For those who want to use branches.

After assigning to a new task on trello named "Task1" make a branch with next command:

git checkout -b feature/Task1

This will make a new branch and switch to it.
Following commits will be done on this branch.

When you finish implementation of task, do final commit, if there is anything to commit, and push.
Push should to be done to a remote branch with a same name (probably this remote branch will be created automaticlly)

Next step is making pull request. The easist way is to use SourceTree. Right click on your branch and the last option in popup menu is create pull request. 
Clicking it will take you to bitbucket page where you just need to add reviews (add all team members) and the merge part is up to them.


**ALSO, every now and then merge stable version of master branch to develop so we can easily revert changes if something goes wrong on master.


Other git usefull commands:		(anyone should add some if needed)

git checkout NAME - switch to branch named NAME






